class Product {

    constructor(name, price, vat, model) {
        this.name = name;
        this.price = price;
        this.vat = vat;
        this.model = model;
    }

    getName() {
        return `${this.name} ${this.model}`;
    }

    getVatPrice() {
        return this.price * this.vat / 100 + this.price;
    }

}

class Laptop extends Product {
    constructor() {
        super(name, price, vat, model);
    }

    delivery() {
        return this.getVatPrice() > 30000 ? 0 : 200;
    }
}

class Tablet extends Product {
    constructor(name, price, vat, model, simCards) {
        super(name, price, vat, model);
        this.simCards = simCards;
    }

    calcPriceOptions() {
        return this.simCards ? this.price + 300 : this.simCards;
    }

    getVatPrice() {
        return this.calcPriceOptions * this.vat / 100 + this.calcPriceOptions;
    }

    delivery() {
        return this.getVatPrice() > 20000 ? 0 : this.getVatPrice() / 100;
    }
}

class Phone extends Product {
    constructor() {
        super(name, price, vat, model, nfc);
        this.nfc = nfc;
    }
    delivery() {
        return this.getVatPrice() > 30000 ? 0 : 200;
    }

}

const laptop1 = new Laptop("Lenovo", 4000, 20, 'thinkPad');
console.log(laptop1);